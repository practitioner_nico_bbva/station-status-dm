import { LitElement } from 'lit-element';
import { setHeader, setSession } from '@practitioner/security-module';
import '@cells-components/cells-generic-dp';

/**
This component es un data manager utilizado para invocar a la API y parsear los datos necesario.

Example:

```html
<station-status-dm></station-status-dm>
```
* @customElement
* @demo demo/index.html
* @extends {LitElement}
*/
export class StationStatusDm extends LitElement {
  static get is() {
    return 'station-status-dm';
  }

  // Declare properties
  static get properties() {
    return {
      stationId: Number,
      host: String
    };
  }

  // Initialize properties
  constructor() {
    super();
    this.host = "http://localhost:1122";
  }

  /**
   * Invoca el servicio de la API para obtener el estado de la estación.
   */
  callService() {
    this._emmit('service_loaded', 'display-block');
    const DP = customElements.get('cells-generic-dp');
    let dp = new DP();
    dp.host = this.host;
    dp.path = "bicycle/stationStatus/" + this.stationId;
    dp.method = "GET"
    dp.timeout = "10000"
    dp.generateRequest()
      .then((res) => this._listSuccess(res))
      .catch(this._listError.bind(this));
  }

  /**
   * 
   * @param {String} eventName nombre del evento
   * @param {*} detail Detalle a  enviar.
   * Es un método de utilidad. 
   */
  _emmit(eventName, detail) {
    this.dispatchEvent(new CustomEvent(eventName, { detail, composed: true, bubbles: true }));
  }

  /**
   * Es el callback de la respuesta satisfactoria.
   * Emite eventos con el detalle y estado.
   * @param {Object} res la respuesta del servicio
   */ 
  _listSuccess(res) {
    this._emmit('list_stations_success_data', res.response.detail);
    this._emmit('service_loaded', 'fade-out');
  }

  /**
   * Es el callback de la respuesta de error.
   * Emite eventos con el detalle y estado.
   * @param {Object} res la respuesta del servicio
   */
  _listError(res) {
    this._emmit('message_response', res.response.message);
    this._emmit('show_message', true);
    this._emmit('service_loaded', 'fade-out');
  }
}

// Register the element with the browser
customElements.define(StationStatusDm.is, StationStatusDm);
